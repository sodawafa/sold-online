mysql = require('./src/service/mysql')

/*
// EXEMPLE
const { Products } = require('./src/dataTypes/db')
mysql = require('./src/service/mysql')
console.log(mysql.query_insert(Products, [Products.id, Products.color], [0, 'red']))
console.log(mysql.query_insert(Products, null, Object.keys(Products)))
console.log(mysql.query_select(Products, [Products.id], 'id=0'))
console.log(mysql.query_select(Products, false, 'id=0'))
console.log(mysql.query_update(Products, [Products.id], ['1'], 'id=0'))
process.exit(0)
*/

/* OLD:
load('user', 5)
load('products', 10)
load('address', 15)
load('remark', 20)
load('order', 25)
load('', 60, true)*/
var timein = 0
var timeDelai = 5
var scripts = [
  'dropTables',
  'createTables',
  'loadFixtures',
  '',
]
scripts.map(
  script => {
    if (script === '') {
      load(false, timein, true)
    } else {
      load(script, timein)
    }
    timein += timeDelai
  },
)

/**
 *
 * @param script - fixture to load
 * @param delai - time in seconds
 * @param isEnd - exit script with
 */
function load (script = '', delai = 0, isEnd = false) {
  setTimeout(() => {
    if (isEnd) {
      mysql.end()
      process.exit(0)
    }
    require('./src/fixtures/' + script)
    //console.log('load ' + './src/fixtures/' + script)
    process.stdout.write('\u001B[0m\n')
  }, delai * 1000 + 100)
}
