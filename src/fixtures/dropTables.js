mysql = require('../service/mysql')
db = require('../dataTypes/db')

const queries = [
  mysql.query_drop(db.Address),
  mysql.query_drop(db.Remark),
  mysql.query_drop(db.Order),
  mysql.query_drop(db.OrderExt),
  mysql.query_drop(db.User),
  mysql.query_drop(db.Products),
  mysql.query_drop(db.NewsProducts),
  mysql.query_drop(db.Clothes),
  mysql.query_drop(db.TypesProducts),
  mysql.query_drop(db.Sales),


]
/*
queries.map(query => {
  mysql.exec(query).then(() => {
    console.info(`table ${query.substr(11)} Deleted`)
  }).catch(e => {
    console.log(e)
  })
})
*/

dropTable = async query => {
  new Promise((resolve, reject) => {
    mysql.exec(query).then(() => {
      console.info(`table ${query.substr(11)} Deleted`)
      resolve()
    }).catch(e => {
      console.log(e)
      reject(e)
    })
  })
}

const asyncFunc = async () => {
  const unresolvedPromises = queries.map(query => dropTable(query));
  await Promise.all(unresolvedPromises);
};
asyncFunc();
