mysql = require('../service/mysql')
db = require('../dataTypes/db')
const { createQueries } = require('../dataTypes/createQuery')

//console.log(queries[0])
createQueries.map(query => {
  mysql.exec(query).then(() => {
    console.info(`table ${query.substr(12,5)} Created`)
  }).catch(e => console.log(e))
})

