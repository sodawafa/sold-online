mysql = require('../service/mysql')
const { Products, User, Address, Remark, Order, NewsProducts ,TypesProducts,Clothes,Sales} = require(
  '../dataTypes/db')
importJson = require('../service/importJson')
insertQuery = require('../dataTypes/insertQuery')

const tables = [
  Products.TABLE,
  User.TABLE,
  Address.TABLE,
  Remark.TABLE,
  Order.TABLE,
  NewsProducts.TABLE,
  TypesProducts.TABLE,
  Clothes.TABLE,
  Sales.TABLE

]
/*tables.map(tableName => {
  importJson(tableName).then(json => {
    const total = json.length - 1
    json.map((data, index) => {
      mysql.exec(queryInsert(tableName, data)).then(() => {
        console.info(`${tableName}: data ${index}/${total} inserted`)
        // if(index==total){
        //   mysql.end()
        // }
      }).catch(e => {
        console.error('error file')
      })
    })
  }).catch(e => {
    console.error('error load json', e)
  })
})*/
insertTableData = async tableName => {
  new Promise((resolve, reject) => {
    importJson(tableName).then(json => {
      const total = json.length - 1
      json.map((data, index) => {
        /*  if (index == 1 && tableName==Products.TABLE) {
            console.log(queryInsert(tableName, data),'')
          }*/
        mysql.exec(insertQuery.callFn(tableName,data)).then(() => {
          console.info(`${tableName}: data ${index}/${total} inserted`)
          resolve()
        }).catch(e => {
          console.log(e)
          reject(e)
        })
      })
    }).catch(e => {
      console.log(e)
      reject(e)
    })
  })
}

const asyncFunc = async () => {
  const unresolvedPromises = tables.map(
    tableName => insertTableData(tableName))
  await Promise.all(unresolvedPromises)
}
asyncFunc()
