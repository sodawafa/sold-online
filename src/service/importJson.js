let fs = require('fs')
const URL_ADDRESS = './src/fixtures/data/'
module.exports = (async (jsonFile) => {
  return new Promise((resolve, reject) => {
    fs.readFile(URL_ADDRESS + jsonFile + '.json', 'utf8', (error, data) => {
      if (error) {
        reject(error)
      } else {
        try {
          resolve(JSON.parse(data))

        } catch (error) {
          reject(error)

        }
      }
    })
  })
})
