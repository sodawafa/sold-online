/**
 * MYSQL
 * */
var mysql = require('mysql')
var db = require('./../dataTypes/db')
var params = require('./../dataTypes/parameters')
var connection = null
db_connection = () => {
  if (!connection) {
    connection = mysql.createConnection({
      host: params.db.host,
      port: params.db.port,
      user: params.db.user,
      password: params.db.password,
      database: params.db.database,
    })
    connection.connect()
  }
  return connection
}
db_end = () => {
  connection.end()
}
db_exec = (async (query) => {
  return new Promise((resolve, reject) => {
    try {
      connection = db_connection()
      connection.query(query, function (error, rows, fields) {
        if (rows && rows.length > 0) {
          if (query.search(`FROM ${db.Products.TABLE}`) !== -1) {
            rows.map((product) => {
              product.availableSizes = JSON.parse(product.availableSizes)
              product.pictures = JSON.parse(product.pictures)
              product.description = JSON.parse(product.description)
            })
          }

          if (query.search(`FROM ${db.NewsProducts.TABLE}`) !== -1) {

            rows.map((newProduct) => {
              newProduct.size = JSON.parse(newProduct.size)
              newProduct.images = JSON.parse(newProduct.images)

            })
          }
          if (query.search(`FROM ${db.Clothes.TABLE}`) !== -1) {

            rows.map((clothe) => {
              clothe.names = JSON.parse(clothe.names)
              clothe.images = JSON.parse(clothe.images)
              clothe.prices = JSON.parse(clothe.prices)

            })
          }
          if (query.search(`FROM ${db.Sales.TABLE}`) !== -1) {

            rows.map((sale) => {
              sale.sizes = JSON.parse(sale.sizes)

            })
          }

        }

        resolve(rows)
        //connection.end()
      })
    } catch (error) {
      reject(error)
    }
  })
})

/**
 * exemple columns = getColumns('Products')
 * @param string
 * @returns {object|null}
 */
getTable = (nameObject) => {
  try {
    const db = require('../dataTypes/db')
    return db[nameObject]
  } catch (error) {
    console.error('error name object', error)
  }
  return null
}

/**
 * exemple columns = getColumns('Products')
 * @param string
 * @returns {string[]}
 */
getColumns = (object) => {
  try {
    let columns = Object.keys(object)
    return columns.filter(column => column !== 'TABLE')
  } catch (error) {
    console.error('error getColumns', error)
  }
  return []
}
/**
 *
 * @param object
 * @param columns []|null
 * @param values []
 * @returns {string}
 */
db_query_insert = (object, columns, values) => {
  try {
    connection = db_connection()
    if (!columns) {
      columns = getColumns(object)
    }
    return `INSERT INTO ${object.TABLE} (${columns.join(
      ',')}) VALUES (${connection.escape(values)});`
  } catch (error) {
    console.error('error query INSERT', error)
  }
  return ''
}
/**
 *
 * @param object
 * @param columns [] | false - *
 * @param condition string | false
 * @returns {string}
 */
db_query_select = (object, columns = false, condition = false) => {
  try {
    connection = db_connection()
    if (!columns) {
      columns = '*'
    } else {
      columns = columns.join(',')
    }
    if (!condition) {
      //  const query = `SELECT * FROM products`
      return `SELECT ${columns} FROM ${object.TABLE};`
    } else {
      return `SELECT ${columns} FROM ${object.TABLE} WHERE (${condition});`
    }
  } catch (error) {
    console.error('error query SELECT', error)
  }
  return ''
}

db_query_update = (
  object, columns = false, values = false, condition = false) => {
  try {
    connection = db_connection()
    let inputs = ''
    values.map((value, index) => {
      inputs += `${columns[index]} = ${(connection.escape(value))}`
    })
    /*
    * `UPDATE user SET token = ${connection.escape(
        accessToken)} WHERE id = ${connection.escape(user.id)}`*/
    if (!condition) {
      return `UPDATE ${object.TABLE} SET ${inputs};`
    } else {
      return `UPDATE ${object.TABLE} SET ${inputs} WHERE (${condition});`
    }
  } catch (error) {
    console.error('error query UPDATE', error)
  }
  return ''
}

/**
 *
 * @param object
 * @returns {string}
 */
db_query_delete = (object, condition = false) => {
  try {
    connection = db_connection()
      if (!condition) {
        return `DELETE FROM ${object.TABLE};`
      } else {
        return `DELETE FROM ${object.TABLE} WHERE ${condition};`
      }

  } catch (error) {
    console.error('error query DELETE', error)
  }
  return ''
}
db_query_drop = (object) => {
  try {
    connection = db_connection()
    return `DROP TABLE ${object.TABLE};`
  } catch (error) {
    console.error('error query DROP', error)
  }
  return ''
}

module.exports = {
  exec: db_exec,
  connection: db_connection,
  end: db_end,
  query_insert: db_query_insert,
  query_select: db_query_select,
  query_drop: db_query_drop,
  query_delete: db_query_delete,
  query_update: db_query_update,
}
