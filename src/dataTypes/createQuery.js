'use strict'
const {
  Products,
  User,
  Address,
  Remark,
  Order,
  OrderExt,
  NewsProducts,
  TypesProducts,
  Clothes,
  Sales

} = require('./db')

const ProductsCreateQuery = `CREATE TABLE ${Products.TABLE}
(
   ${Products.id}             int(10)      NOT NULL AUTO_INCREMENT,
   ${Products.name}           varchar(255) NOT NULL,
   ${Products.picture}        varchar(255) NOT NULL,
   ${Products.pictureHover}   varchar(255) NOT NULL,
   ${Products.pictures}       JSON         NOT NULL,
   ${Products.color}          varchar(255) NOT NULL,
   ${Products.matter}         varchar(255) NOT NULL,
   ${Products.reference}      varchar(255) NOT NULL,
   ${Products.price}          varchar(255) NOT NULL,
   ${Products.availableSizes} varchar(255) NOT NULL,
   ${Products.description}    varchar(255) NOT NULL,
   ${Products.unit}           varchar(255) NOT NULL,
   ${Products.createdAt}      datetime     NOT NULL,
   PRIMARY KEY (${Products.id})
);`
const AddressCreateQuery = `CREATE TABLE ${Address.TABLE}
(
   ${Address.id}         int(10) NOT NULL AUTO_INCREMENT,
   ${Address.firstName} varchar(255) NOT NULL,
   ${Address.lastName}  varchar(255) NOT NULL,
   ${Address.society}   varchar(255) NOT NULL,
   ${Address.VATNumber} varchar (255) NOT NULL ,
   ${Address.address} varchar (255) NOT NULL ,
   ${Address.postalCode} int (10) NOT NULL ,
   ${Address.town}      varchar(255) DEFAULT NULL,
   ${Address.state}      varchar(255) DEFAULT NULL,
   ${Address.country}      varchar(255) DEFAULT NULL,
   ${Address.telephone}      varchar(255) DEFAULT NULL,
   PRIMARY KEY (${Address.id})
);`
const OrderCreateQuery = `CREATE TABLE ${Order.TABLE}
(
   ${Order.id}     INT(10) NOT NULL AUTO_INCREMENT,
   ${Order.id_user}      INT,
   ${Order.status}      INT DEFAULT '0',
   ${Order.date}  DATETIME NOT NULL,
   ${Order.unit}         VARCHAR(255) NOT NULL DEFAULT '€',
   PRIMARY KEY (${Address.id}),
   FOREIGN KEY (${Order.id_user}) REFERENCES ${User.TABLE} (${User.id})
);`
const OrderExtCreateQuery = `CREATE TABLE ${OrderExt.TABLE}
(
   ${OrderExt.id}     INT(10) NOT NULL AUTO_INCREMENT,
   ${OrderExt.id_order}   INT,
   ${OrderExt.id_product}   INT,
   ${OrderExt.quantity}     INT(10) NOT NULL DEFAULT '0',
   ${OrderExt.buying_price} INT(10) NOT NULL,
   ${OrderExt.updated_at}  DATETIME NOT NULL,
   PRIMARY KEY (${OrderExt.id}),
   FOREIGN KEY (${OrderExt.id_order}) REFERENCES ${Products.TABLE}(${Order.id}),
   FOREIGN KEY (${OrderExt.id_product}) REFERENCES ${Products.TABLE}(${Products.id})
);`

const UserCreateQuery = `CREATE TABLE ${User.TABLE}
(
   ${User.id}         int(10) NOT NULL AUTO_INCREMENT,
   PRIMARY KEY (${User.id}),
   ${User.username}   varchar(255) NOT NULL,
   ${User.firstName} varchar(255) NOT NULL,
   ${User.lastName}  varchar(255) NOT NULL,
   ${User.birthday}   date         DEFAULT NULL,
   ${User.password}   varchar(255) NOT NULL,
   ${User.email} varchar (255) NOT NULL ,
   ${User.status} varchar (255) NOT NULL ,
   ${User.picture} varchar (255) NOT NULL ,
   ${User.token}  varchar(255) DEFAULT NULL
  
);`

const RemarkCreateQuery = `CREATE TABLE ${Remark.TABLE}
(
   ${Remark.id}     int(10) NOT NULL AUTO_INCREMENT,                            
   ${Remark.remark} varchar(255) NOT NULL,
   ${Remark.date}   datetime     NOT NULL,
   PRIMARY KEY (${Remark.id})
);`
const NewsProductsCreateQuery = `CREATE TABLE ${NewsProducts.TABLE}
(
   ${NewsProducts.id}     int(10) NOT NULL AUTO_INCREMENT,                            
   ${NewsProducts.name} varchar(255) NOT NULL,
   ${NewsProducts.picture} varchar(255) NOT NULL,
   ${NewsProducts.pictureHover} varchar(255) NOT NULL,
    ${NewsProducts.images} JSON NOT NULL,
   ${NewsProducts.price} varchar(255) NOT NULL,
   ${NewsProducts.size} varchar(255) NOT NULL,
   ${NewsProducts.category} varchar(255) NOT NULL,
   ${NewsProducts.color} varchar(255) NOT NULL,
   ${NewsProducts.composition} varchar(255) NOT NULL,
   ${NewsProducts.matter} varchar(255) NOT NULL,
   ${NewsProducts.season} varchar(255) NOT NULL,
   ${NewsProducts.ref} varchar(255) NOT NULL,
   ${NewsProducts.promo} varchar(255) NOT NULL,
   ${NewsProducts.newPrice} varchar(255) NOT NULL,
   ${NewsProducts.retail} varchar(255) NOT NULL,
    PRIMARY KEY (${NewsProducts.id})
);`
const TypesProductsCreateQuery = `CREATE TABLE ${TypesProducts.TABLE}
(
  ${TypesProducts.id}     int(10) NOT NULL AUTO_INCREMENT, 
  ${TypesProducts.type} varchar(255) NOT NULL,
  ${TypesProducts.picture} varchar(255) NOT NULL,
    PRIMARY KEY (${TypesProducts.id})
);`
const ClothesCreateQuery = `CREATE TABLE ${Clothes.TABLE}
(
   ${Clothes.id}     int(10) NOT NULL AUTO_INCREMENT,                            
   ${Clothes.name} varchar(255) NOT NULL,
   ${Clothes.color} varchar(255) NOT NULL,
   ${Clothes.price} varchar(255) NOT NULL,
   ${Clothes.availability} varchar(255) NOT NULL,
   ${Clothes.picture} varchar(255) NOT NULL,
   ${Clothes.type} INT,
   ${Clothes.images} varchar(255) NOT NULL,
   ${Clothes.names}  LONGTEXT NOT NULL,
   ${Clothes.prices} varchar(255) NOT NULL,
   ${Clothes.title} varchar(255) NOT NULL,
   ${Clothes.size} varchar(255) NOT NULL,
    PRIMARY KEY (${Clothes.id}),
    FOREIGN KEY (${Clothes.type}) REFERENCES ${TypesProducts.TABLE} (${TypesProducts.id})
);`
const SalesCreateQuery = `CREATE TABLE ${Sales.TABLE}
(
  ${Sales.id}     int(10) NOT NULL AUTO_INCREMENT, 
  ${Sales.name} varchar(255) NOT NULL,
  ${Sales.picture} varchar(255) NOT NULL,
  ${Sales.hover} varchar(255) NOT NULL,
  ${Sales.price} varchar(255) NOT NULL,
  ${Sales.sizes} varchar(255) NOT NULL,
    PRIMARY KEY (${Sales.id})
);`

module.exports = {
  UserCreateQuery,
  ProductsCreateQuery,
  AddressCreateQuery,
  RemarkCreateQuery,
  OrderCreateQuery,
  OrderExtCreateQuery,
  NewsProductsCreateQuery,
  TypesProductsCreateQuery,
  ClothesCreateQuery,
  SalesCreateQuery,
  createQueries: [
    ProductsCreateQuery,
    AddressCreateQuery,
    UserCreateQuery,
    RemarkCreateQuery,
    OrderCreateQuery,
    OrderExtCreateQuery,
    NewsProductsCreateQuery,
    TypesProductsCreateQuery,
    ClothesCreateQuery,
    SalesCreateQuery,
  ],
}
