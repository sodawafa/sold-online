'use strict'
module.exports = {
  db: Object.freeze({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'sold-online',
  }),
}
