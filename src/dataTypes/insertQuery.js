'use strict'
const mysql = require('../service/mysql')

const {
  Products,
  User,
  Address,
  Remark,
  Order,
  OrderExt,
  NewsProducts,
  TypesProducts,
  Clothes,
  Sales,

} = require('./db')
const autoID = 'NULL'
const ProductsInsertQuery = (data) => mysql.query_insert(
  Products,
  [
    Products.id,
    Products.name,
    Products.picture,
    Products.pictureHover,
    Products.pictures,
    Products.price,
    Products.availableSizes,
    Products.description,
    Products.unit,
    Products.color,
    Products.matter,
    Products.reference,
    Products.createdAt],
  [
    autoID,
    data.name,
    data.picture,
    data.pictureHover,
    JSON.stringify(data.pictures),
    data.price,
    JSON.stringify(data.availableSizes),
    JSON.stringify(data.description),
    data.unit,
    data.color,
    data.matter,
    data.reference,
    data.createdAt,
  ],
)
const UserInsertQuery = (data) => mysql.query_insert(
  User,
  [
    User.id,
    User.username,
    User.firstName,
    User.lastName,
    User.birthday,
    User.email,
    User.password,
    User.status,
    User.picture,
    User.token],
  [
    autoID,
    data.username,
    data.firstName,
    data.lastName,
    data.birthday,
    data.email,
    data.password,
    data.status,
    data.picture,
    data.token,
  ],
)
const AddressInsertQuery = (data) => mysql.query_insert(
  Address,
  [
    Address.id,
    Address.firstName,
    Address.lastName,
    Address.society,
    Address.VATNumber,
    Address.address,
    Address.postalCode,
    Address.town,
    Address.state,
    Address.country,
    Address.telephone,

  ],
  [
    autoID,
    data.firstName,
    data.lastName,
    data.society,
    data.VATNumber,
    data.address,
    data.postalCode,
    data.town,
    data.state,
    data.country,
    data.telephone,
  ],
)
const RemarkInsertQuery = (data) => mysql.query_insert(
  Remark,
  [
    Remark.id,
    Remark.remark,
    Remark.date,
  ],
  [
    autoID,
    data.remark,
    data.date,
  ],
)
const OrderInsertQuery = (data) => mysql.query_insert(
  Order,
  [
    autoID,
    Order.id,
    Order.id_user,
    Order.id_product,
    Order.quantity,
    Order.buying_price,
    Order.code_promo,
    Order.buying_date,
    Order.unit,
  ],
  [
    data.id_user,
    data.id_product,
    data.quantity,
    data.buying_price,
    data.code_promo,
    data.buying_date,
    data.unit,
  ],
)
const NewsProductsInsertQuery = (data) => mysql.query_insert(
  NewsProducts,
  [
    NewsProducts.id,
    NewsProducts.name,
    NewsProducts.picture,
    NewsProducts.pictureHover,
    NewsProducts.images,
    NewsProducts.price,
    NewsProducts.size,
    NewsProducts.category,
    NewsProducts.color,
    NewsProducts.composition,
    NewsProducts.matter,
    NewsProducts.season,
    NewsProducts.ref,
    NewsProducts.promo,
    NewsProducts.newPrice,
    NewsProducts.retail,
  ],
  [
    autoID,
    data.name,
    data.picture,
    data.pictureHover,
    JSON.stringify(data.images),
    data.price,
    JSON.stringify(data.size),
    data.category,
    data.color,
    data.composition,
    data.matter,
    data.season,
    data.ref,
    data.promo,
    data.newPrice,
    data.retail,
  ],
)
const ClothesInsertQuery = (data) => mysql.query_insert(
  Clothes,
  [
    Clothes.id,
    Clothes.name,
    Clothes.color,
    Clothes.price,
    Clothes.availability,
    Clothes.picture,
    Clothes.type,
    Clothes.images,
    Clothes.names,
    Clothes.prices,
    Clothes.title,
    Clothes.size,
  ],
  [
    autoID,
    data.name,
    data.color,
    data.price,
    data.availability,
    data.picture,
    data.type,
    JSON.stringify(data.images),
    JSON.stringify(data.names),
    JSON.stringify(data.prices),
    data.title,
    JSON.stringify(data.size),

  ],
)
const TypesProductsInsertQuery = (data) => mysql.query_insert(
  TypesProducts,
  [
    TypesProducts.id,
    TypesProducts.type,
    TypesProducts.picture,

  ],
  [
    data.id,
    data.type,
    data.picture,

  ],
)
const SalesInsertQuery = (data) => mysql.query_insert(
  Sales,
  [
    Sales.id,
    Sales.name,
    Sales.picture,
    Sales.hover,
    Sales.price,
    Sales.sizes,
  ],
  [
    autoID,
    data.name,
    data.picture,
    data.hover,
    data.price,
    JSON.stringify(data.sizes),

  ],
)
const insertQuery = (table, data) => {
  switch (table) {
    case   Products.TABLE:
      return ProductsInsertQuery(data)
    case   User.TABLE:
      return UserInsertQuery(data)
    case   Address.TABLE:
      return AddressInsertQuery(data)
    case   Remark.TABLE:
      return RemarkInsertQuery(data)
    case   Order.TABLE:
      return OrderInsertQuery(data)
    case   NewsProducts.TABLE:
      return NewsProductsInsertQuery(data)
    case   Clothes.TABLE:
      return ClothesInsertQuery(data)
    case   TypesProducts.TABLE:
      return TypesProductsInsertQuery(data)
    case Sales.TABLE:
      return SalesInsertQuery(data)
    // OrderExt,
  }
  return ''
}
module.exports = {
  UserInsertQuery,
  ProductsInsertQuery,
  AddressInsertQuery,
  RemarkInsertQuery,
  OrderInsertQuery,
  //OrderExtInsertQuery,
  NewsProductsInsertQuery,
  TypesProductsInsertQuery,
  SalesInsertQuery,
  callFn: insertQuery,
}
