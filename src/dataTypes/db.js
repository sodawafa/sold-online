'use strict'

const Products = {
  TABLE: 'products',
  id: 'id',
  name: 'name',
  picture: 'picture',
  pictureHover:'pictureHover',
  pictures: 'pictures',
  color: 'color',
  matter: 'matter',
  reference: 'reference',
  price: 'price',
  availableSizes: 'availableSizes',
  description: 'description',
  unit: 'unit',
  createdAt: 'createdAt',
}

const Address = {
  TABLE: 'address',
  id: 'id',
  firstName: 'firstName',
  lastName: 'lastName',
  society: 'society',
  VATNumber: 'VATNumber',
  address: 'address',
  postalCode: 'postalCode',
  town: 'town',
  state: 'state',
  country: 'country',
  telephone: 'telephone',
}

const Order = {
  TABLE: 'orders',
  id: 'id',
  id_user: 'id_user',
  status: 'status',
  date: 'date',
  unit: 'unit',
}

const OrderExt = {
  TABLE: 'order_ext',
  id: 'id',
  id_order: 'id_order',
  id_product: 'id_product',
  quantity: 'quantity',
  buying_price: 'buying_price',
  updated_at: 'updated_at',
}

const Remark = {
  TABLE: 'remark',
  id: 'id',
  remark: 'remark',
  date: 'date',
}

const User = {
  TABLE: 'user',
  id: 'id',
  username: 'username',
  firstName: 'firstName',
  lastName: 'lastName',
  birthday: 'birthday',
  password: 'password',
  email: 'email',
  status: 'status',
  picture: 'picture',
  token: 'token',
}
const NewsProducts = {
  TABLE: 'newsProducts',
  id: 'id',
  name: 'name',
  picture: 'picture',
  pictureHover:'pictureHover',
  images: 'images',
  price: 'price',
  size: 'size',
  category: 'category',
  color: 'color',
  composition: 'composition',
  matter: 'matter',
  season: 'season',
  ref: 'ref',
  promo: 'promo',
  newPrice: 'newPrice',
  retail: 'retail',


}
const TypesProducts ={
  TABLE: 'typesProducts',
  id: 'id',
  type: 'type',
  picture: 'picture',

}
const Clothes = {
  TABLE: 'clothes',
  id: 'id',
  name: 'name',
  color: 'color',
  price: 'price',
  availability: 'availability',
  picture: 'picture',
  type: 'type',
  images: 'images',
  names: 'names',
  prices: 'prices',
  title: 'title',
  size: 'size',


}
const Sales = {
  TABLE: 'sales',
  id: 'id',
  name: 'name',
  picture: 'picture',
  hover: 'hover',
  price: 'price',
  sizes: 'sizes',


}


module.exports = {
  Products: Object.freeze(Products),
  Address: Object.freeze(Address),
  Order: Object.freeze(Order),
  OrderExt: Object.freeze(OrderExt),
  Remark: Object.freeze(Remark),
  User: Object.freeze(User),
  NewsProducts: Object.freeze(NewsProducts),
  TypesProducts: Object.freeze(TypesProducts),
  Clothes: Object.freeze(Clothes),
  Sales: Object.freeze(Sales),

 /*TABLES: Object.freeze({
    User: 'User',
    Products: 'Products',
    Address: 'Address',
    Remark: 'Remark',
    Order: 'Order',
    OrderExt: 'OrderExt',
  }),*/
}
