const versionApi = '/api/v1'
const express = require('express')
const cors = require('cors')

const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser')
const flash = require('express-flash')
const session = require('express-session')
const nodemailer = require('nodemailer')
const randtoken = require('rand-token')
const bcrypt = require('bcrypt')
const logger = require('morgan')
const createError = require('http-error')
const db = require('./src/dataTypes/db')
const mysql = require('./src/service/mysql')
const { request, response } = require('express')
const app = express()
app.use(cors())
app.use(cookieParser())
app.use(session({
  secret: '123458cat',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000 },
}))
app.use(flash())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/public', express.static('public'))
//app.use('/images', express.static(__dirname + '/public/new-product'));

//  mysql = require('./src/service/mysql')

const generateAccessToken = (user) => {
  return jwt.sign({ id: user.id }, 'ACCESS_TOKEN_SECRET')
}
const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization
  console.log('ccccccc', authHeader)
  if (authHeader) {
    const token = authHeader.split('Bearer')[1].trim()
    jwt.verify(token, 'ACCESS_TOKEN_SECRET', (error, user) => {
      if (error) {
        return res.status(403).send('user not authenticate')
      }
      console.log('ccc', user)
      req.user = user
      next()
    })
  } else {
    res.status(401).send('error')
  }
}
searchUser = async (username, password) => {
  // verifier si l'utilisateur est enregistré dans la BD et!! mot de passe correcte
  return new Promise((resolve, reject) => {
    try {
      /*const query = `SELECT *
                           FROM user
                           WHERE username LIKE "${username}"`*/
      const query = mysql.query_select(db.User, false,
        `username LIKE "${username}"`)
      console.log('sql', username, password)

      mysql.exec(query).then((user) => {
        user = user[0]//convertir reponse BD de tableau vers object
        console.log(user)
        if (user.password === password) {
          resolve(user)
        }
        reject(false)
      }).catch(error => {
        reject(error)
      })
    } catch (error) {
      reject(error)
    }
  })
}
app.post(`${versionApi}/login`, (request, response) => {
  // recuperer les parametres envoyer par html
  console.log(request.body)
  let username = request.body.username
  let password = request.body.password
  // recherche le nom dans la BD
  searchUser(username, password).then(
    user => {
      // creer un token = ID unique aleatoire
      const accessToken = generateAccessToken(user)
      // const refreshToken = generateRefreshToken(user);
      // refreshTokens.push(refreshToken)
      //enregistre token dans la BD
      /* const query =`UPDATE user SET token = ${connection.escape(
         accessToken)} WHERE id = ${connection.escape(user.id)}`*/
      const query = mysql.query_update(db.User, [db.User.token], [accessToken],
        `id = ${mysql.connection().escape(user.id)}`)
      mysql.exec(query).then(() => {
        // envoyer token
        response.json({
          success: true,
          accessToken,
          // refreshToken,
          user,
        })
      }).catch(error => {
        response.status(401).send('error!')
      })
    },
  ).catch(error => {
    console.log('err')
    response.status(401).send('wrong password!')
  })
})
app.post(`${versionApi}/signUp`, (req, res, next) => {
  const username = req.body.username
  const firstName = req.body.firstName
  const lastName = req.body.lastName
  const birthday = req.body.birthday
  const email = req.body.email
  const password = req.body.password
  const status = req.body.status
  const picture = req.body.picture
  /* const query = mysql.query_insert(db.User, false,
     ["NULL", username, firstName, lastName, birthday, password, email, status,picture])*/
  mysql.exec(mysql.query_insert(db.User, false, [
    'NULL',
    username,
    firstName,
    lastName,
    birthday,
    password,
    email,
    status,
    picture])).then(() => {
    res.json({
      success: true,
    })
  }).catch(error => {
    res.status(401).send('error registered')
  })
  // db_exec(`INSERT INTO user(username,firstName,lastName,birthday,email,password,status)VALUES (
  // ${connection.escape(username)},
  // ${connection.escape(firstName)},
  // ${connection.escape(lastName)},
  // ${connection.escape(birthday)},
  // ${connection.escape(email)},
  // ${connection.escape(password)},
  // ${connection.escape(status)})`).then(() => {
  //   res.json({
  //     success: true,
  //   })
  // }).catch(error => {
  //   res.status(401).send('error registered')
  // })
})
//send email
sendEmail = async (email, token) => {
  return new Promise((resolve, reject) => {
    try {
      let mail = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'wefedev@gmail.com',
          pass: 'Wafa@2021',
        },
      })
      console.log('eee', email)
      let mailOptions = {
        from: 'wefedev@gmail.com',
        to: email,
        subject: 'Reset Password Link - wafa.com',
        html: `<p>You requested for reset password, kindly use this <a href="http://localhost:3000/reset/${token}">http://localhost:3000/reset</a>to reset your password</p>`,

      }
      mail.sendMail(mailOptions, (error, response) => {
        if (error) {
          console.error(error)
          reject.json({
            success: false,
            error: error,
          })
        } else {
          console.error('success mail!')
          resolve.json({
            success: true,
          })
        }

      })
    } catch (error) {
      console.error(error)
    }
  })
}

/* send reset password link in email */
app.post(`${versionApi}/reset-password-email`, (req, res, next) => {
  const email = req.body.email
  const generateAccessToken = (user) => {
    return jwt.sign({ id: user.id }, 'ACCESS_TOKEN_SECRET')
  }
  const query = mysql.query_select(db.User, false, `email=${email}`)
  /* const query = `SELECT * FROM user WHERE email = ${connection.escape(email)}`*/
  mysql.exec(query).then((data) => {
    let type = ''
    let msg = ''
    if (data.length > 0) {
      let user = data[0]
      let token = generateAccessToken(user)
      let sent = sendEmail(email, token)
      if (sent !== '0') {
        /*    let data = {
              token:token
            }*/
        const query = mysql.query_update(db.User, false, `email=${email}`)
        /*  const query = `UPDATE user SET token=${connection.escape(
            token)} WHERE email =${connection.escape(email)}`*/
        mysql.exec(query).then((response) => {
          console.log('bbbb', response)

        })
        type = 'success'
        msg = 'The reset password link has been sent to your email address'

      } else {
        type = 'error'
        msg = 'Something goes to wrong. Please try again'
      }

    } else {
      console.log('2')
      type = 'error'
      msg = 'The Email is not registered with user'
    }
    req.flash(type, msg)
    //res.redirect('/');
    res.json({
      success: true,
      type, msg,
    })
  })
})
app.post(`${versionApi}/update-password`, (req, res, next) => {
  let token = req.body.token
  let password = req.body.password
  // `SELECT * FROM user WHERE token = ${connection.escape(token)} `
  const query = mysql.query_select(db.User, false, `token=${token}`)
  mysql.exec(query).
    then((user) => {
      let type
      let msg
      if (user.length > 0) {
        // let saltRounds = 4
        /* bcrypt.genSalt(saltRounds, function (err, salt) {
           bcrypt.hashSync(password, salt, function (err, hash) {
             let data = {
               password: hash
             }*/
        const query = mysql.query_update(db.User, false, `password=${password}`,
          `id=${user[0].id}`)

        /*     const query = `UPDATE user SET password=${connection.escape(
               password)} WHERE id = ${connection.escape(user[0].id)}`*/
        mysql.exec(query).then((response) => {
          console.log(response)

        })

        //})
        //})
        type = 'success'
        msg = 'Your password has been updated successfully'
      } else {
        console.log('2')
        type = 'no success'
        msg = 'Invalid link; please try again'
      }
      /*req.flash(type, msg);*/
      res.json({
        type, msg,
      })

    })
})
//get products
app.get(`${versionApi}/products`, (request, response) => {
  const query = mysql.query_select(db.Products, false, false)

  mysql.exec(query).then(data => {
    console.log(data)

    response.json({
      success: true,
      data: data,
    })
  }).catch(error => {
    response.json({
      success: false,

    })
  })
})
//get news products
app.get(`${versionApi}/news-products`, ((req, res, next) => {
  const query = mysql.query_select(db.NewsProducts, false, false)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
}))
//get clothes
app.get(`${versionApi}/clothes`, (req, res, next) => {
  const query = mysql.query_select(db.Clothes, false, false)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//get clothe
app.get(`${versionApi}/clothe/:id`, (req, res, next) => {
  const id = req.params.id
  const query = mysql.query_select(db.Clothes, false, `id=${id}`)
  mysql.exec(query).then(data => {
    if (data.length > 0) {
      res.json({
        success: true,
        data: data[0],
      })
    } else {
      res.json({
        success: false,
      })
    }
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//get types
app.get(`${versionApi}/types`, (req, res, next) => {
  const query = mysql.query_select(db.TypesProducts, false, false)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//get sales
app.get(`${versionApi}/sales`, (req, res, next) => {
  const query = mysql.query_select(db.Sales, false, false)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//bar de filter
app.get(`${versionApi}/sales/filter`, (req, res, next) => {

const KEY = 0,VALUE = 1, ASC='ASC', DESC='DESC'
  let search = false//['name', 'MAMA B - MAGLIA - 400309 - NERO']
  let min = false//['price', DESC]
  let max = ['price', ASC]
  //let orderBy = ['price', ASC]
  let orderBy = ['name', ASC]

  let query = `SELECT * FROM sales`
  if (search) {
    if (query.indexOf('where ') != -1) {
      query += ` and ${search[KEY]} = "${search[VALUE]}"`
    } else {
      query += ` where ${search[KEY]} = "${search[VALUE]}"`
    }
  }
  if (min) {
    if (query.indexOf('where ') != -1) {
      query += ` and ${min[KEY]} > "${min[VALUE]}"`
    } else {
      query += ` where ${min[KEY]} > "${min[VALUE]}"`
    }
  }
  if (max) {
    if (query.indexOf('where ') != -1) {
      query += ` and ${max[KEY]} < "${max[VALUE]}"`
    } else {
      query += ` where ${max[KEY]} < "${max[VALUE]}"`
    }
  }
  if (orderBy) {
    query += ` ORDER BY ${orderBy[KEY]} ${orderBy[VALUE]}`
  }
 //res.send(query)
  mysql.exec(query).then((data) => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//get type
app.get(`${versionApi}/type/:id`, (req, res, next) => {
  const id = req.params.id
  const query = mysql.query_select(db.TypesProducts, false, `id=${id}`)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data[0],
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
app.get(`${versionApi}/productsByType/:type`, (req, res, next) => {
  const type = req.params.type
  const query = mysql.query_select(db.Clothes, false, `type=${type}`)
  mysql.exec(query).then(data => {
    res.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
// get new product
app.get(`${versionApi}/productNew/:id`, (req, res, next) => {
  const id = req.params.id
  const query = mysql.query_select(db.NewsProducts, false, `id= ${id}`)
  mysql.exec(query).then(data => {
    if (data.length > 0) {
      res.json({
        success: true,
        data: data[0],
      })
    } else {
      res.json({
        success: false,
      })
    }
  }).catch(() => {
    res.json({
      success: false,
    })
  })
})
//get product
app.get(`${versionApi}/product/:id`, (request, response) => {
  const id = request.params.id
  /*  const query = `SELECT * FROM products WHERE id=${connection.escape(id)}`*/
  const query = mysql.query_select(db.Products, false, `id= ${id}`)
  mysql.exec(query).then(data => {
    if (data.length > 0) {
      response.json({
        success: true,
        data: data[0],
      })
    } else {
      response.json({
        success: false,
      })
    }
  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//get users
app.get(`${versionApi}/users`, (req, res, next) => {
  const query = mysql.query_select(db.User, false)
  mysql.exec(query).then(data => {
    console.log('success', data)
    res.json({
      success: true,
      data: data,
    })
  }).catch(error => {
    res.json({
      success: false,
    })
  })
})
//get user
app.get(`${versionApi}/user/:id`, (request, response) => {
  const id = request.params.id
  const query = mysql.query_select(db.User, false, `id=${id}`)
  // const query = `SELECT * FROM user WHERE id=${id}`
  mysql.exec(query).then(data => {
    if (data.length > 0) {
      response.json({
        success: true,
        data: data[0],
      })
    }
  }).catch(() => {
    response.json({
      success: false,
    })

  })
})
app.post(`${versionApi}/user/connected`,
  (request, response, next) => {
    const token = request.body.token
    console.log('rrr', request.body.token)
    const query = mysql.query_select(db.User, false, `token='${token}'`)
    // const query = `SELECT *
    //                FROM user
    //                WHERE token = ${connection.escape(token)}`
    mysql.exec(query).then(data => {
      console.log('eee', data,query)
      if (data.length > 0) {
        response.json({
          success: true,
          token,
          data: data[0],
        })
      } else {
        response.json({
          success: false,
        })
      }

    }).catch(error => {
      response.json({
        success: false,
      })
    })

  })
//get profile
app.get(`${versionApi}/profile`, (request, response) => {
  //const id = request.params.id
  const query = mysql.query_select(db.User, false)
  // const query = `SELECT * FROM user WHERE id=${connection.escape(
  //   request.user.id)}`
  mysql.exec(query).then(users => {
    console.log('dzer', users)
    response.json({
      success: true,
      user: users[0],
    })

  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//edit user
app.post(`${versionApi}/edit/user/:id`, (request, response) => {
  const id = request.params.id
  const username = request.body.username
  const firstName = request.body.firstName
  const lastName = request.body.lastName
  const birthday = request.body.birthday
  const email = request.body.email
  const status = request.body.status
  const query = mysql.query_update(db.User, false,
    [`${username}, ${firstName}, ${lastName}, ${birthday}, ${email}, ${status}`],
    `id=${id}`)
  /*  const query = `UPDATE user SET
    username=${connection.escape(username)},
    firstName=${connection.escape(firstName)},
    lastName=${connection.escape(lastName)},
    birthday=${connection.escape(birthday)},
    email=${connection.escape(email)},
    status=${connection.escape(status)} WHERE id=${connection.escape(id)};`*/
  mysql.exec(query).then(() => {
    console.log('ser', query)
    response.json({
      success: true,
    })
  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//add address
app.post(`${versionApi}/addAddress`, (request, response) => {
  const firstName = request.body.firstName
  const lastName = request.body.lastName
  const society = request.body.society
  const VATNumber = request.body.VATNumber
  const address = request.body.address
  const postalCode = request.body.postalCode
  const town = request.body.town
  const state = request.body.state
  const country = request.body.country
  const telephone = request.body.telephone
  const query = mysql.query_insert(db.Address, false, [
    'NULL',
    firstName,
    lastName,
    society,
    VATNumber,
    address,
    postalCode,
    town,
    state,
    country,
    telephone])
  /*  const query = `INSERT INTO address (firstName,lastName,society,VATNumber,address,postalCode,town,state,country,telephone)VALUES (
    ${mysql.connection.escape(firstName)},
    ${mysql.connection.escape(lastName)},
    ${mysql.connection.escape(society)},
    ${mysql.connection.escape(VATNumber)},
    ${mysql.connection.escape(address)},
    ${mysql.connection.escape(postalCode)},
    ${mysql.connection.escape(town)},
    ${mysql.connection.escape(state)},
    ${mysql.connection.escape(country)},
    ${mysql.connection.escape(telephone)})`*/
  mysql.exec(query).then(() => {
    console.log('wafa', query)
    response.json({
      success: true,
    })
  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//get message
app.get(`${versionApi}/get/message`, (request, response) => {
  const query = mysql.query_select(db.Remark, false, false)
  mysql.exec(query).then(data => {
    response.json({
      success: true,
      data: data,
    })
  }).catch(() => {
    response.json({
      success: false,
    })
  })

})
//send message
app.post(`${versionApi}/message`, (request, response) => {
  const remark = request.body.remark
  const date = new Date()
  const query = mysql.query_insert(db.Remark, false, ['NULL', remark, date])
  /*`INSERT INTO remark(remark,date)VALUES (
${mysql.connection(remark)},
${mysql.connection(date)})`*/
  console.log('eee', query)
  mysql.exec(query).then(() => {
    response.json({
      success: true,
    })
  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//delete comment
app.delete(`${versionApi}/delete/remark/:id`, (request, response) => {
  const id = request.params.id
  const query = mysql.query_delete(db.Remark, `id=${id}`)
  //const query = `DELETE FROM remark WHERE id=${id}`
  mysql.exec(query).then(() => {
    console.log('cat', query)
    response.json({
      success: true,
    })
  }).catch(() => {
    response.json({
      success: false,
    })
  })
})
//add cart
app.post(`${versionApi}/cart`, (request, response) => {
  const id_product = request.body.id_product
  const id_user = request.body.id_user
  const buying_price = request.body.buying_price
  const unit = request.body.unit
  const quantity = parseInt(request.body.quantity)
  const id = request.body.id
  if (id) {
    const query = `SELECT * FROM orders WHERE id=${id}`
    mysql.exec(query).then(order => {
      if (!order) {
        const query = `INSERT INTO orders(
            id,
            id_user,
            id_product,
            quantity,
            buying_price,
            unit
                   
        )VALUES (
                 ${mysql.connection.escape(id)},
                 ${mysql.connection.escape(id_user)},
                 ${mysql.connection.escape(id_product)},
                 ${mysql.connection.escape(quantity)},
                 ${mysql.connection.escape(buying_price)},
                 ${mysql.connection.escape(unit)})`
        mysql.exec(query).then(() => {
          response.json({
            success: true,
          })
        }).catch(() => {
          response.json({
            success: false,
          })
        })
        if (order) {
          const query = `UPDATE orders SET 
         id_product=${mysql.connection.escape(id_product)},
         quantity=${mysql.connection.escape(quantity)},
         buying_price=${mysql.connection.escape(buying_price)},
         unit=${mysql.connection.escape(unit)}`
          mysql.exec(query).then(() => {
            response.json({
              success: true,
            })
          }).catch(() => {
            response.json({
              success: false,
            })
          })
        }
      }
    })
  }

})

app.listen(3003)



